//
//  AppDelegate.h
//  BMNavigationController
//
//  Created by BirdMichael on 2018/5/18.
//  Copyright © 2018年 birdmichael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

